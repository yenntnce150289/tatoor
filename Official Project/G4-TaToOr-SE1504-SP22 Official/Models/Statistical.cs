﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G4_TaToOr_SE1504_SP22_Official.Models
{
    public class Statistical
    {
        public DateTime value1 { get; set; }
        public DateTime value2 { get; set; }
    }
    public class StatisticalDetail 
    {
        public double Product_ID { get; set; }
        public string Product_Name { get; set; }
        public double Product_Quantity { get; set; }
        public double Product_TotalMoneyOfProduct { get; set; }

    }
    public class DetailBoom: StatisticalDetail
    {
        public double User_ID { get; set; }
    }
}