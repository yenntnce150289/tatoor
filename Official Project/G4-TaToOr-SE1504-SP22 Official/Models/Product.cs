//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace G4_TaToOr_SE1504_SP22_Official.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Product
    {
        public Product()
        {
            this.CartDetails = new HashSet<CartDetail>();
            this.Pictures = new HashSet<Picture>();
            this.Reviews = new HashSet<Review>();
            this.Tastes = new HashSet<Taste>();
        }
        [Display(Name = "Product")]
        public double Product_ID { get; set; }

        [Display(Name = "Name")]
        public string Product_Name { get; set; }

        [Display(Name = "Price")]
        public Nullable<double> Product_Price { get; set; }

        [Display(Name = "Description")]
        public string Product_Description { get; set; }

        [Display(Name = "Star")]
        public Nullable<double> Product_Star { get; set; }

        [Display(Name = "Status")]
        public Nullable<int> ProductStatus_ID { get; set; }

        [Display(Name = "Origin")]
        public int Origin_ID { get; set; }

        [Display(Name = "Type Of Food")]
        public int TypeOfFood_ID { get; set; }

        [Display(Name = "Form Of Food")]
        public int FormOfFood_ID { get; set; }

        public virtual ICollection<CartDetail> CartDetails { get; set; }
        public virtual FormOfFood FormOfFood { get; set; }
        public virtual Origin Origin { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
        public virtual ProductStatu ProductStatu { get; set; }
        public virtual TypeOfFood TypeOfFood { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Taste> Tastes { get; set; }
    }
}
