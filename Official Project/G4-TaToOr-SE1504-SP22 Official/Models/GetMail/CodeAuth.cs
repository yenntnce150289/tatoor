﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G4_TaToOr_SE1504_SP22_Official.Models.GetMail
{
    public class CodeAuth
    {
        /**
        *
        *
        * Constructor using for get random number and account info to send mail with itself
*/
        public CodeAuth(string acc_Username, string code, string acc_Email)
        {
            Acc_Username = acc_Username;
            Code = code;
            Acc_Email = acc_Email;
        }
        public string Acc_Username { get; set; }
        public string Code { get; set; }
        public string Acc_Email { get; set; }
    }
}