﻿using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace G4_TaToOr_SE1504_SP22_Official.Models.GetMail
{
    public class SendMailService
    {
        public static MailSettings mailSettings;

        public SendMailService()
        {
            mailSettings = new MailSettings();
        }

        /**
        *
        * @author
        * Task using for send mail with mail content
        *
*/
        public async Task SendMail(MailContent mailContent)
        {
            var email = new MimeMessage();
            email.Sender = new MailboxAddress(mailSettings.DisplayName, mailSettings.Mail);
            email.From.Add(new MailboxAddress(mailSettings.DisplayName, mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(mailContent.To));
            email.Subject = mailContent.Subject;


            var builder = new BodyBuilder();
            builder.HtmlBody = mailContent.Body;
            email.Body = builder.ToMessageBody();

            // use SmtpClient of MailKit
            var smtp = new MailKit.Net.Smtp.SmtpClient();

            try
            {
                smtp.Connect(mailSettings.Host, mailSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(mailSettings.Mail, mailSettings.Password);
                await smtp.SendAsync(email);
            }
            catch (Exception ex)
            {
                //Send mail failed, the content of mail will be save to mailssave
                //System.IO.Directory.CreateDirectory("mailssave");
                //var emailsavefile = string.Format(@"mailssave/{0}.eml", Guid.NewGuid());
                //await email.WriteToAsync(emailsavefile);
                Console.WriteLine(ex);
            }

            smtp.Disconnect(true);
        }

        /**
        *
        * @
        * Method using for call Task SendMail with mail Content
        *
*/
        public async void SendMailAsync(MailContent mailContent)
        {
            await SendMail(mailContent);
        }
    }
}