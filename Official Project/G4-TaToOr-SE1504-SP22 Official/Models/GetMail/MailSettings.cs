﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G4_TaToOr_SE1504_SP22_Official.Models.GetMail
{
    public class MailSettings
    {
        /**
        *
        * @author 
        * Constructor using for get mail settings of sender's mail and host of this mail service
*/
        public MailSettings()
        {
            Mail = "nguyenthingocyenntn@gmail.com";
            DisplayName = "TaToOr";
            Password = "01206876067";
            Host = "smtp.gmail.com";
            Port = 587;
        }

        public MailSettings(string mail, string displayName, string password, string host, int port)
        {
            Mail = mail;
            DisplayName = displayName;
            Password = password;
            Host = host;
            Port = port;
        }

        public string Mail { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}