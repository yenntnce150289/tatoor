﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G4_TaToOr_SE1504_SP22_Official.Models.GetMail
{
    public class MailContent
    {
        /**
        *
        * 
        * Method using for get mailing address, subject and body of mail
*/

        public string To { get; set; }              // Mailing address
        public string Subject { get; set; }         // Subject of mail
        public string Body { get; set; }            // Body or content of mail

        public MailContent(string to, string subject, string body)
        {
            To = to;
            Subject = subject;
            Body = body;
        }
    }
}