﻿using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
