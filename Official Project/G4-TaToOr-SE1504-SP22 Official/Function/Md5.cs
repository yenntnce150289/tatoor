﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace G4_TaToOr_SE1504_SP22_Official.Function
{
    public class Md5
    {
        public string GetMd5(String password)
        {
            if (string.IsNullOrEmpty(password)) return null;
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(password);
            byte[] targetData = md5.ComputeHash(fromData);
            string passwordMd5 = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                passwordMd5 += targetData[i].ToString("x2");

            }
            return passwordMd5;

        }
        public string GetHash(string plainText)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            // Compute hash from the bytes of text
            // Get hash result after compute it
            byte[] result = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(plainText));

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
                // 'x2' format string: 0d = hexadecimal
            }

            return strBuilder.ToString();
        }
    }
}