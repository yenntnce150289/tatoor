﻿using G4_TaToOr_SE1504_SP22_Official.Function;
using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class UserController : Controller
    {
        TaToOrEntities context = new TaToOrEntities();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [HttpPost]
        public ActionResult ChangePassWord()
        {

            var user = new User();
            Md5 md5 = new Md5();

            string oldpass = Request["oldpassword"];
            string newpass = Request["newpassword"];
            string cfpass = Request["cfpassword"];
            Debug.WriteLine("============================================" + oldpass);
            Debug.WriteLine("============================================" + newpass);
            Debug.WriteLine("============================================" + cfpass);
            ViewBag.ErrorPass = "==========ViewBag.ErrorPass===========";



            if (oldpass == "" || newpass == "" || cfpass == "")
            {

                return RedirectToAction("EditPersonalInfo", "User", new { message = "Please do not emty" });
            }
            else
            {
                oldpass = md5.GetMd5(oldpass);


                var oldpassVSpass = context.Users.FirstOrDefault(a => a.User_Password == oldpass);
                // Debug.WriteLine("=================dbpass===========================" + dbpass);

                Debug.WriteLine("================old vs pass============================" + oldpassVSpass);
                if (oldpassVSpass != null)
                {
                    if (cfpass != newpass)
                    {

                        RedirectToAction("EditPersonalInfo", "User");
                        return RedirectToAction("EditPersonalInfo", "User", new { message = "Password and Confirmation Password not match." });
                    }
                    else
                    {
                        double iduser = 0;
                        iduser = Convert.ToDouble(Session["id"]);
                        var olduser = context.Users.Find(iduser);
                        cfpass = md5.GetMd5(cfpass);
                        olduser.User_Password = cfpass;
                        context.Configuration.ValidateOnSaveEnabled = false;
                        context.SaveChanges();
                        ViewBag.ErrorPass = "Change PassWord Successful";
                        return RedirectToAction("EditPersonalInfo", "User", new { message = "Change PassWord Successful!" });
                    }
                }
                else
                {
                    ViewBag.ErrorPass = "Oldpass wrong";
                    return RedirectToAction("EditPersonalInfo", "User", new { message = "Oldpass wrong!" });

                }
            }

            //return RedirectToAction("EditPersonalInfo", "User");
        }

        // GET: User/Edit/5
        public ActionResult EditPersonalInfo(string message)
        {
            ViewBag.ErrorPass = message;


            double iduser = 0;
            var user = new User();
            iduser = Convert.ToDouble(Session["id"]);
            ViewBag.iduser = iduser;
            if (iduser == 0) //Filter
            {

                return RedirectToAction("Login", "Login");
            }
            else
            {
                
                user = context.Users.Find(iduser); // lấy user bằng id
                user.User_Name = user.User_Name.Trim(); //Xoá dấu cách dư của User_Name
                user.User_Email = user.User_Email.Trim();//Xoá dấu cách dư của User_email
                user.User_PhoneNumber = user.User_PhoneNumber.Trim();//Xoá dấu cách dư của SĐT
                ViewBag.Year = user.User_DateOfBirth.ToString("yyyy-MM-dd");//Truyền giá trị Ngày tháng năm sinh bằng ViewBag bởi vì phải ép kiểu mới hiển thị được
                if (user.User_Address != null) // Vì ban đầu đăng ký sẽ trống address nên bắt buộc có hàm này để xoá dấu cách nếu ko sẽ lỗi.
                {
                    user.User_Address = user.User_Address.Trim();//Xoá dấu cách dư của Address
                }

            }
            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult EditPersonalInfo(User user)
        {
            try
            {
                // TODO: Add update logic here

                //Lấy user cũ về từ Database 
                var olduser = context.Users.Find(user.User_ID);
                // Gáng các giá trị cũ bằng giá trị mới từ web gửi về qua model bao gồm: Sđt, Địa Chỉ, Tên, Email, Sinh Nhật, Giới Tính
                olduser.User_PhoneNumber = user.User_PhoneNumber;
                olduser.User_Address = user.User_Address;
                olduser.User_Name = user.User_Name;
                olduser.User_DateOfBirth = user.User_DateOfBirth;
                olduser.GenderOfUser_ID = user.GenderOfUser_ID;
                ViewBag.Year = user.User_DateOfBirth.ToString("yyyy-MM-dd"); // truyền giá trị ngày tháng năm sinh qua ViewBag 
                context.SaveChanges(); // lệnh lưu dữ liệu database
                ViewBag.Status = "Successfully updated"; // Nếu thành công thì truyền thông báo bằng Viewbag
                return View(user);
            }
            catch
            {
                ViewBag.Status = "The system is busy, please try again later!!!"; // Nếu thất bại thì truyền thông báo bằng Viewbag
                return View(user);
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
