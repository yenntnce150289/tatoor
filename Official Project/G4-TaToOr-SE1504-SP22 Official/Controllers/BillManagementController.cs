﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G4_TaToOr_SE1504_SP22_Official.Models;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class BillManagementController : Controller
    {
        // GET: BillManagement
        TaToOrEntities tatoor = new TaToOrEntities();
        public ActionResult ShowAllBill()
        {

            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                ViewBag.B = tatoor.Bills.ToList();
                ViewBag.T = tatoor.Bills.Count();
                ViewBag.C = tatoor.Carts.ToList();
                double id = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = id;

                ViewBag.Processing = tatoor.Bills.Where(model => model.BillStatus_ID == 0).ToList();
                ViewBag.Delivery = tatoor.Bills.Where(model => model.BillStatus_ID == 1).ToList();
                ViewBag.Recived = tatoor.Bills.Where(model => model.BillStatus_ID == 2).ToList();
                ViewBag.Cancel = tatoor.Bills.Where(model => model.BillStatus_ID == 3).ToList();
                ViewBag.NotRe = tatoor.Bills.Where(model => model.BillStatus_ID == 4).ToList();
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }

           
        }

        // GET: BillManagement/Edit/5
        public ActionResult EditBillStatus(int id)
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                var editting = new TaToOrEntities().Bills.Find(id);
                Session["ID_Bill"] = id;
                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                return View(editting);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }


            
        }

        // POST: BillManagement/Edit/5
        [HttpPost]
        public ActionResult EditBillStatus(Bill b)
        {
            try
            {
                var context = new TaToOrEntities();
                var editing = context.Bills.Find(Session["ID_Bill"]);
                editing.BillStatus_ID = b.BillStatus_ID;
                context.SaveChanges();
                var getUserID = tatoor.Carts.Find(editing.Cart_ID).User_ID;// lấy userid từ billid qua cart
                var checkProID = tatoor.CartDetails.Where(a => a.Cart_ID == editing.Cart_ID).ToList();// lấy danh sách card detail từ cartid 
                foreach (var i in checkProID)
                {
                    var checkRe = tatoor.Reviews.Where(item => item.User_ID == getUserID && item.Product_ID == i.Product_ID).ToList();// lấy list reveiw
                    if (checkRe.Count() > 0)
                    {
                        checkRe.FirstOrDefault().ReviewEdit_ID = 1;// đổi thành được review
                        tatoor.SaveChanges();// lưu
                    }
                }
                return RedirectToAction("ShowAllBill");
            }
            catch
            {
                return View();
            }
        }



        // GET: BillManagement/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BillManagement/Create
        public ActionResult Create()
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }


           
        }

        // POST: BillManagement/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: BillManagement/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BillManagement/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}