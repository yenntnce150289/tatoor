﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class CartDetailController : Controller
    {
        TaToOrEntities context = new TaToOrEntities();
        // GET: CartDetail
        public ActionResult Index()
        {
            return View();
        }

        // GET: CartDetail/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CartDetail/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CartDetail/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CartDetail/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CartDetail/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult UpdateCartDetail(int id)
        {
            return View();
        }

        // POST: CartDetail/Edit/5
        [HttpPost]
        public ActionResult UpdateCartDetail(CartDetailUpdate cartlist)
        {
            try
            {
                var GetNewValue = cartlist.cartDetails.ToList();
              for(int i = 0; i < GetNewValue.Count; i++)
                {
                    var newcartdetail = GetNewValue[i];
                    var olditem = context.CartDetails.Find(newcartdetail.CartDetail_ID);
                    olditem.CartDetail_QuantityProduct = newcartdetail.CartDetail_QuantityProduct;
                    olditem.CartDetail_TotalOfProduct = newcartdetail.CartDetail_QuantityProduct * olditem.Product.Product_Price;
                    context.SaveChanges();
                }
                // TODO: Add update logic here

                return RedirectToAction("ShowCartDetail");
            }
            catch
            {
                return View();
            }
        }
        //GET: CartDetail/Delete/5
        public ActionResult Delete(double id)
        {
            double cartid = Convert.ToDouble(Session["cartid"]);
            var cartdetails = context.CartDetails.ToList();
            for (int i = 0; i < cartdetails.Count; i++)
            {
                if (cartdetails[i].Cart_ID == cartid && cartdetails[i].Product_ID == id)
                {
                    context.CartDetails.Remove(cartdetails[i]);
                    context.SaveChanges();
                }
            }
            return RedirectToAction("ShowCartDetail");
        }

        // POST: CartDetail/Delete/5
       
        public ActionResult ShowCartDetail()
        {
            //Session["ProductID"] = 0;
            double cartid = Convert.ToDouble(Session["cartid"]);
            var cartdetails = context.CartDetails.ToList();
            List<CartDetail> listCartdetailofUser = new List<CartDetail>();
            for (int i = 0; i < cartdetails.Count; i++)
            {
                if (cartdetails[i].Cart_ID == cartid)
                {
                    listCartdetailofUser.Add(cartdetails[i]);

                }
            }
            ViewBag.PIC = context.Pictures.ToList(); // move picture to views
            CartDetailUpdate list = new CartDetailUpdate();
            list.cartDetails = listCartdetailofUser;
            return View(list);
        }
        [HttpPost]
        public ActionResult ShowCartDetail(List<CartDetail> cart)
        {
            return View();
        }
    }
}
