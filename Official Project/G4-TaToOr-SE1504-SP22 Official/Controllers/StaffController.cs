﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class StaffController : Controller
    {
        TaToOrEntities entities = new TaToOrEntities();
        // GET: Staff
        public ActionResult StaffIndex()  // trang index cua staff hiển thị thông tin về sản phẩm
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                var listProduct = new TaToOrEntities().Products.ToList();
                ViewBag.All = entities.Products.Count();
                double id = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = id;
                ViewBag.SoldOutProduct = listProduct.Where(model => model.ProductStatus_ID == 0).ToList();
                ViewBag.AvailableProduct = listProduct.Where(model => model.ProductStatus_ID == 1).ToList();
                ViewBag.DeletedProduct = listProduct.Where(model => model.ProductStatus_ID == 3).ToList();

                return View(listProduct);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }


            
        }


        // GET: Staff/Edit/5
        public ActionResult EditStaff(string message)
        {
            ViewBag.ErrorPass = message;


            double iduser = 0;
            var user = new User();
            iduser = Convert.ToDouble(Session["id"]);
            ViewBag.iduser = iduser;
            if (iduser == 0) //Filter
            {

                return RedirectToAction("Login", "Login");
            }
            else
            {

                user = entities.Users.Find(iduser); // lấy user bằng id

                user.User_Email = user.User_Email.Trim();//Xoá dấu cách dư của User_email
                user.User_PhoneNumber = user.User_PhoneNumber.Trim();//Xoá dấu cách dư của SĐT
                ViewBag.Year = user.User_DateOfBirth.ToString("yyyy-MM-dd");//Truyền giá trị Ngày tháng năm sinh bằng ViewBag bởi vì phải ép kiểu mới hiển thị được
                if (user.User_Address != null) // Vì ban đầu đăng ký sẽ trống address nên bắt buộc có hàm này để xoá dấu cách nếu ko sẽ lỗi.
                {
                    user.User_Address = user.User_Address.Trim();//Xoá dấu cách dư của Address
                }

            }
            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult EditStaff(User user)
        {
            try
            {
                // TODO: Add update logic here

                //Lấy user cũ về từ Database 
                var olduser = entities.Users.Find(user.User_ID);
                // Gáng các giá trị cũ bằng giá trị mới từ web gửi về qua model bao gồm: Sđt, Địa Chỉ, Tên, Email, Sinh Nhật, Giới Tính
                olduser.User_PhoneNumber = user.User_PhoneNumber;
                olduser.User_Address = user.User_Address;
                olduser.User_DateOfBirth = user.User_DateOfBirth;
                ViewBag.Year = user.User_DateOfBirth.ToString("yyyy-MM-dd"); // truyền giá trị ngày tháng năm sinh qua ViewBag 
                entities.SaveChanges(); // lệnh lưu dữ liệu database
                ViewBag.Status = "Successfully updated"; // Nếu thành công thì truyền thông báo bằng Viewbag
                return View(user);
            }
            catch
            {
                ViewBag.Status = "The system is busy, please try again later!!!"; // Nếu thất bại thì truyền thông báo bằng Viewbag
                return View(user);
            }
        }
        // GET: ProductManagement/Edit/5


        public ActionResult StaffCreateNewProduct()
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }



           
        }

        // POST: ProductManagement/Create
        [HttpPost]
        public ActionResult StaffCreateNewProduct(Product model1)
        {
            try
            {
                // TODO: Add insert logic here
                var context = new TaToOrEntities();
                context.Products.Add(model1);
                float idcount = context.Products.Count();
                model1.Product_ID = idcount + 1;
                model1.Product_Star = 0;
                model1.ProductStatus_ID = 1;
                context.SaveChanges();
                return RedirectToAction("StaffIndex");
            }
            catch
            {
                return View();
            }


        }
        [HttpGet]
        public ActionResult StaffUpload(int id)
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                ViewBag.Pic = entities.Pictures.Where(i => i.Product_ID == id).ToList();
                ViewBag.idPro = id;
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }



            //var context = new TaToOrEntities();
            //var editing = context.Products.Find(id);

            
        }


        [HttpGet]
        public ActionResult StaffUploadImage(int id)
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                Session["ID_Product"] = id;
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }



        }

        public ActionResult StaffEditProduct(int id)
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                var context = new TaToOrEntities();
                var editing = context.Products.Find(id);
                editing.Product_Name = editing.Product_Name.Trim();
                editing.Product_Description = editing.Product_Description.Trim();

                return View(editing);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }



        }

        // POST: ProductManagement/Edit/5
        [HttpPost]
        public ActionResult StaffEditProduct(Product model)
        {
            try
            {
                // TODO: Add update logic here
                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                var context = new TaToOrEntities();
                var editing = context.Products.Find(model.Product_ID);
                editing.Product_Name = model.Product_Name;
                editing.Product_Price = model.Product_Price;
                editing.Product_Description = model.Product_Description;
                editing.Product_Star = model.Product_Star;
                editing.ProductStatus_ID = model.ProductStatus_ID;
                editing.Origin_ID = model.Origin_ID;
                editing.TypeOfFood_ID = model.TypeOfFood_ID;
                editing.FormOfFood_ID = model.FormOfFood_ID;
                context.SaveChanges();
                return RedirectToAction("StaffIndex");
            }
            catch
            {
                return View();
            }
        }



        [HttpPost]
        public ActionResult StaffUploadImage(Picture img)
        {
            var context = new TaToOrEntities();
            //var editing = context.Products.Find(Convert.ToInt32(ID));

            string filename = Path.GetFileNameWithoutExtension(img.ImageFile.FileName);
            string extension = Path.GetExtension(img.ImageFile.FileName);
            filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
            img.Picture_Link = "../../img/" + filename;
            filename = Path.Combine(Server.MapPath("../../img/"), filename);
            img.ImageFile.SaveAs(filename);
            img.Product_ID = Convert.ToInt32(Session["ID_Product"]);
            float idcount = context.Pictures.Count();

            img.Picture_ID = idcount + 1;
            context.Pictures.Add(img);
            context.SaveChanges();



            ModelState.Clear();
            return RedirectToAction("StaffUpload/" + Session["ID_Product"]);
        }

        public ActionResult StaffDeleteProduct(int id)
        {

            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                var context = new TaToOrEntities();
                var deleteing = context.Products.Find(id);
                return View(deleteing);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }



           
        }

        // POST: ProductManagement/Delete/5
        [HttpPost]
        public ActionResult StaffDeleteProduct(Product model)
        {
            try
            {
                // TODO: Add delete logic here
                var context = new TaToOrEntities();
                var olditem = context.Products.Find(model.Product_ID);
                olditem.ProductStatus_ID = 3;
                context.SaveChanges();
                return RedirectToAction("StaffIndex");
            }
            catch
            {
                return View();
            }
        }
    }
}