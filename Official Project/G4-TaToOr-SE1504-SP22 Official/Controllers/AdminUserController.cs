﻿using G4_TaToOr_SE1504_SP22_Official.Function;
using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class AdminUserController : Controller
    {
        TaToOrEntities context = new TaToOrEntities();
        // GET: AdminUser
        public ActionResult UserManagerIndex(string status)
        {
            // 18 - 22 Filter User, phải là Admin ( 0 ) và id user khác 0 
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if (typeofuser == 0 && userid != 0)
            {
                ViewBag.Status = status;
                var userlist = context.Users.ToList();
                double countDeleted = 0;
                double countAdmin = 0;
                double countStaff = 0;
                double countUser = 0;
                // Vòng lặp đếm các loại tài khoản
                foreach (var get in userlist)
                {
                    if (get.TypeOfUser_ID == 0)
                    {
                        countAdmin += 1;
                    }
                    else if (get.TypeOfUser_ID == 1)
                    {
                        countStaff += 1;
                    }
                    else if (get.TypeOfUser_ID == 2)
                    {
                        countUser += 1;
                    }
                    else { countDeleted += 1; }

                }
                ViewBag.CountDeleted = countDeleted;
                ViewBag.CountAdmin = countAdmin;
                ViewBag.CountStaff = countStaff;
                ViewBag.CountUser = countUser;
                //Hàm tính % trong tổng số
                ViewBag.PercentAdmin = Math.Round(countAdmin / userlist.Count * 100, 2, MidpointRounding.ToEven);
                ViewBag.PercentDeleted = Math.Round(countDeleted / userlist.Count * 100, 2, MidpointRounding.ToEven);
                ViewBag.PercentStaff = Math.Round(countStaff / userlist.Count * 100, 2, MidpointRounding.ToEven);
                ViewBag.PercentUser = Math.Round(countUser / userlist.Count * 100, 2, MidpointRounding.ToEven);
                return View(userlist);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }
        }

        // GET: AdminUser/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        public ActionResult ThongKeTest()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ThongKeTest(Statistical a)
        {

            if (a.value1 < a.value2) // điều kiện nếu giá trị ngày kết thúc ( value 2 ) nhỏ hơn ngày bắt đầu (value 1) thì chạy tập lệnh dướ đây
            {
                DateTime ngaybatdau = a.value1; // Gán giá trị ngày bắt đầu và ngày kết thúc
                DateTime ngayketthuc = a.value2;
                //danh sách lấy ngày hợp lệ giữa ngày bắt đầu và kết thúc
                var listBillAvailable = context.Bills.Where(model => model.Bill_Date >= ngaybatdau && model.Bill_Date <= ngayketthuc).ToList(); // dùng LinQ lấy giá trị giữa ngày bắt đầu và ngày kết thúc
                var listDetailBillAvailable = new List<BillDetail>(); // tạo 1 danh sách chứa các chi tiết của từng đơn
                var listStatisticalDetail = new List<StatisticalDetail>();
                for (int i = 0; i < listBillAvailable.Count; i++)
                {
                    double billID = listBillAvailable[i].Bill_ID;
                    //Danh sách của từng thành phần chi tiết trong đơn hàng chi tiết ( ví dụ Bill ID là 1 có 3 cart detail thì danh sách ngày có 3 phần tử )
                    var listNumberBillDetailAvailable = context.BillDetails.Where(model => model.Bill_ID == billID).ToList();
                    for (int j = 0; j < listNumberBillDetailAvailable.Count; j++)
                    {
                        //thêm từng sản phẩm vào list đó.
                        listDetailBillAvailable.Add(listNumberBillDetailAvailable[j]);
                    }
                }


                for (int i = 0; i < listDetailBillAvailable.Count; i++)
                {

                    string ProductNameiList = listDetailBillAvailable[i].CartDetail.Product.Product_Name.Trim();
                    string Product_Name = listDetailBillAvailable[i].CartDetail.Product.Product_Name.Trim();
                    double Product_Quantity = (double)listDetailBillAvailable[i].CartDetail.CartDetail_QuantityProduct;
                    double Product_TotalMoneyOfProduct = (double)listDetailBillAvailable[i].CartDetail.CartDetail_TotalOfProduct;
                    for (int j = 0; j < i; j++)
                    {
                        // vòng lặp kiểm tra xem có món ăn trùng nhau ko, nếu có tự động vào
                        string ProductNamejList = listDetailBillAvailable[j].CartDetail.Product.Product_Name.Trim();
                        if (ProductNameiList.Equals(ProductNamejList))
                        {
                            Product_Name = ProductNameiList;
                            Product_Quantity += (double)listDetailBillAvailable[j].CartDetail.CartDetail_QuantityProduct;
                            Product_TotalMoneyOfProduct += (double)listDetailBillAvailable[j].CartDetail.CartDetail_TotalOfProduct;
                        }
                    }

                    //if (listStatisticalDetail.Count == 0)
                    //{
                    StatisticalDetail n = new StatisticalDetail();
                    n.Product_Name = Product_Name;
                    n.Product_Quantity = Product_Quantity;
                    n.Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                    listStatisticalDetail.Add(n);
                    //}
                    //else
                    //{
                    //    StatisticalDetail n = new StatisticalDetail();
                    //    n.Product_Name = ProductNameiList;
                    //    n.Product_Quantity = Product_Quantity;
                    //    n.Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                    //    for (int k = 0; k < listStatisticalDetail.Count; k++)
                    //    {
                    //        double availablecount = 0;
                    //        if (listStatisticalDetail[k].Product_Name.Equals(ProductNameiList) == true)
                    //        {
                    //            listStatisticalDetail[k].Product_Quantity = Product_Quantity;
                    //            listStatisticalDetail[k].Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                    //        }
                    //        else if (listStatisticalDetail[k].Product_Name.Equals(ProductNameiList) == false)
                    //        {
                    //            availablecount += 1;
                    //        }
                    //        if (availablecount == k+1)
                    //        {
                    //            listStatisticalDetail.Add(n);
                    //        }
                    //    }
                    //}
                }

                //Sản phẩm bán nhiều nhất: 
                listStatisticalDetail = listStatisticalDetail.GroupBy(model => new { model.Product_Name, model.Product_Quantity, model.Product_TotalMoneyOfProduct }).Select(model => model.First()).ToList();
                listStatisticalDetail = listStatisticalDetail.OrderBy(model => model.Product_Quantity).Reverse().ToList(); // sort theo số lượng
                double BestSeller_Quantity = listStatisticalDetail[0].Product_Quantity;
                double DoanhThu = 0;

                for (int i = 0; i < listStatisticalDetail.Count; i++)
                {
                    DoanhThu += listStatisticalDetail[i].Product_TotalMoneyOfProduct;
                }
                ViewBag.Find = listDetailBillAvailable;
                ViewBag.Detail = listStatisticalDetail;
                ViewBag.Quantity = BestSeller_Quantity;
                ViewBag.value1 = a.value1;
                ViewBag.value2 = a.value2;
                ViewBag.DoanhThu = DoanhThu;
            }

            return View();
        }
        // GET: AdminUser/Create
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateUserbyAdmin()
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if (typeofuser == 0 && userid != 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }
        }
        // POST: AdminUser/Create
        [HttpPost]
        public ActionResult CreateUserbyAdmin(User model)
        {
            try
            {
                double typeofuser = 4;
                typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
                double userid = 0;
                userid = Convert.ToDouble(Session["id"]);
                if (typeofuser == 0 && userid != 0)
                {
                    var checkAccount = context.Users.FirstOrDefault(s => s.User_Account == model.User_Account);
                    var checkMail = context.Users.FirstOrDefault(s => s.User_Email == model.User_Email);

                    if (checkAccount == null && checkMail == null)
                    {

                        double idCount = context.Users.Count(); // lấy tổng người dùng
                        Md5 mahoa = new Md5();
                        model.User_Password = mahoa.GetHash(model.User_Password); // Chuyển pass người dùng nhập thành MD5 và gáng vào database
                        model.User_ID = idCount + 1; // id người mới tự động +1
                        context.Users.Add(model); // Thêm vào database
                        context.SaveChanges(); // lưu lại
                        return RedirectToAction("UserManagerIndex", "AdminUser", new { status = "Create User * " + model.User_Account.Trim() + " * Complete" });  // Thông báo thành công
                    }
                    else
                    {
                        if (checkAccount != null)
                        {
                            ViewBag.error = "Account name already exists";
                        }
                        else
                        {
                            ViewBag.errorMail = "Account email already exists";
                        }

                        return View();

                    }
                }
                else
                {
                    return RedirectToAction("SOS", "Home");
                }
            }
            catch
            {
                ViewBag.Status = "There some problem when create user ... Please try again!!!";// Thông báo thất bại
                return View();
            }
        }

        // GET: AdminUser/Edit/5
        public ActionResult EditUserbyAdmin(double id)
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if (typeofuser == 0 && userid != 0)
            {
                var getuser = context.Users.Find(id);
                getuser.User_Name = getuser.User_Name.Trim();
                if (getuser.User_Email != null)
                {
                    getuser.User_Email = getuser.User_Email.Trim();
                }
                if (getuser.User_Address != null)
                {
                    getuser.User_Address = getuser.User_Address.Trim();
                }
                if (getuser.User_Note != null)
                {
                    getuser.User_Note = getuser.User_Note.Trim();
                }
                getuser.User_DateOfBirth = getuser.User_DateOfBirth;
                ViewBag.Year = getuser.User_DateOfBirth.ToString("yyyy-MM-dd");
                if (getuser.User_PhoneNumber != null)
                {
                    getuser.User_PhoneNumber = getuser.User_PhoneNumber.Trim();
                }

                return View(getuser);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }
        }

        // POST: AdminUser/Edit/5
        [HttpPost]
        public ActionResult EditUserbyAdmin(User model)
        {
            try
            {

                // TODO: Add update logic here
                var getUser = context.Users.Find(model.User_ID);
                getUser.TypeOfUser_ID = model.TypeOfUser_ID;
                getUser.GenderOfUser_ID = model.GenderOfUser_ID;
                getUser.User_Name = model.User_Name;
                getUser.User_Email = model.User_Email;
                getUser.User_Note = model.User_Note;
                getUser.User_Address = model.User_Address;
                getUser.User_DateOfBirth = model.User_DateOfBirth;
                getUser.User_PhoneNumber = model.User_PhoneNumber;
                context.SaveChanges();
                ViewBag.Status = "Edit Successful";

                return RedirectToAction("UserManagerIndex", "AdminUser", new { status = "Edit User * " + model.User_Account.Trim() + " * Complete" });

            }
            catch
            {
                return RedirectToAction("UserManagerIndex", "AdminUser", new { status = "Edit User * " + model.User_Account.Trim() + " * Fail" });
                //return View();
            }
        }

        // GET: AdminUser/Delete/5
        public ActionResult Delete(double id)
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if (typeofuser == 0 && userid != 0)
            {
                // TODO: Add delete logic here
                var getUser = context.Users.Find(id); // Tìm User theo ID người dùng đã chọn
                if (getUser.TypeOfUser_ID == 0)
                {
                    return RedirectToAction("UserManagerIndex", "AdminUser", new { status = "Deleted User *" + getUser.User_Account.Trim() + " * is FAIL, because account you delete is Admin" }); // thông báo thất bại
                }
                else
                {
                    getUser.TypeOfUser_ID = 3; // chuyển thành số 3, deleted
                    context.Configuration.ValidateOnSaveEnabled = false;
                    context.SaveChanges();
                    return RedirectToAction("UserManagerIndex", "AdminUser", new { status = "Deleted User *" + getUser.User_Account.Trim() + " * Complete" }); // thông báo thành công}

                }
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }

        }


    }
}
