﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G4_TaToOr_SE1504_SP22_Official.Function;
using G4_TaToOr_SE1504_SP22_Official.Models;
using Newtonsoft.Json;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class RegisterController : Controller
    {
        TaToOrEntities entities = new TaToOrEntities();

        public ActionResult Registration()
        {
            Session["Register"] = 1;
            return View();
        }
        [HttpPost]
        public ActionResult Registration(User u)
        {
            int getRegister = Convert.ToInt32(Session["Register"]);


            var check = entities.Users.FirstOrDefault(s => s.User_Account == u.User_Account);
            var checkMail = entities.Users.FirstOrDefault(s => s.User_Email == u.User_Email);

            if (check == null && checkMail == null)
            {
                Md5 mahoa = new Md5();
                u.User_Password = mahoa.GetHash(u.User_Password);
                entities.Configuration.ValidateOnSaveEnabled = false;
                entities.Users.Add(u);
                var context = entities;
                float idcount = context.Users.Count();
                u.User_ID = idcount + 1;
                u.User_Address = "  ";
                u.TypeOfUser_ID = 2;
                //var subject = "Chào mừng bạn đã đến với TaTorOr";
                //var htmlMessage = "Cảm ơn bạn đã quan tâm TaToOr. Giờ đây bạn có thể đăng nhập tài khoản để mua hàng tại TaToOr";
                //MailContent content = new MailContent(u.User_Email, subject, htmlMessage);
                //SendMailService sendMail = new SendMailService();
                //sendMail.SendMailAsync(content);
                entities.SaveChanges();

                // string code = await Re
                return RedirectToAction("Index", "Home");

            }
            else
            {
                if (getRegister == 1)
                {
                    if (check != null)
                    {
                        ViewBag.error = "Account name already exists";
                    }
                    else
                    {
                        ViewBag.errorMail = "Account email already exists";
                    }

                    return View();
                }
                else
                {
                    if (check != null)
                    {
                        ViewBag.Status = "Account name already exists";
                        return RedirectToAction("GoogleRegister", "Register", new { message = "Account name already exists" });  // Thông báo thành công
                    }
                    else
                    {
                        ViewBag.Status = "Account email already exists";
                        return RedirectToAction("GoogleRegister", "Register", new { message = "Account email already exists" });  // Thông báo thành công
                    }
                }


            }

        }

        [HttpPost]
        public ActionResult GoogleResponse(string info)
        {
            try
            {
                Session["Register"] = 2;
                var acc = new User();
                acc = (User)JsonConvert.DeserializeObject(info, acc.GetType());
                Session["InfoAcc"] = acc;

                return RedirectToAction("GoogleRegister");


            }
            catch (Exception)
            {

                return RedirectToAction("Registration");
            }


        }


        // GET: Register
        public ActionResult GoogleRegister(string message)
        {
            ViewBag.Status = message;
            var acc = (User)Session["InfoAcc"];


            var check = entities.Users.FirstOrDefault(s => s.User_Email == acc.User_Email);

            if (check != null)
            {

                Session["TypeOfUser"] = check.TypeOfUser_ID.ToString();
                Session["UserName"] = check.User_Name.ToString();
                Session["Name"] = check.User_Account.ToString();
                Session["id"] = check.User_ID.ToString();

                var listCart = entities.Carts.ToList();
                List<Cart> ifAvailable = new List<Cart>();
                double User_id = Convert.ToDouble(Session["id"]);
                Cart cart = new Cart();
                for (int i = 0; i < listCart.Count; i++)
                {
                    var item = listCart[i];
                    if (item.User_ID == User_id)
                    {
                        ifAvailable.Add(item);
                    }
                }
                if (ifAvailable.Count == 0)
                {
                    entities.Carts.Add(cart);
                    cart.Cart_ID = listCart.Count + 1;
                    cart.User_ID = User_id;
                    Debug.WriteLine("tao gio hang" + cart.Cart_ID + "\n\n\n");
                    Session["cartid"] = listCart.Count + 1;
                    entities.SaveChanges();
                }
                else if (ifAvailable.Count != 0)
                {
                    for (int i = 0; i < ifAvailable.Count; i++)
                    {
                        if (ifAvailable.Count == i + 1)
                        {
                            Session["cartid"] = ifAvailable[i].Cart_ID;
                        }
                    }
                }
                return RedirectToAction("Index", "Home");

            }

            return View(acc);
        }


        //Toiws daay thoi
        [HttpPost]
        public ActionResult GoogleRegister(User us)
        {
          
            return View();
        }
    }
}
