﻿using G4_TaToOr_SE1504_SP22_Official.Function;
using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        TaToOrEntities entities = new TaToOrEntities();
        public ActionResult Login()
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if (userid == 0)
            {

                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }






        }
        [HttpPost]
        public ActionResult Login(string User_Account, string User_Password)
        {
            Md5 md5 = new Md5();
            User_Account = User_Account.Trim().ToLower();
            User_Password = User_Password.Trim().ToLower();
            // Chuyển pass mới nhập thành MD5
            User_Password = md5.GetMd5(User_Password);
            if (ModelState.IsValid)
            {
                //Hàm check UserName và password xem có khớp trong database
                var lg = entities.Users.FirstOrDefault(a => a.User_Account == User_Account && a.User_Password == User_Password);

                if (lg != null && lg.TypeOfUser_ID != 3)// Nếu nó khác null tức nghĩa là thành công và check xem account đó có bị xoá hay không
                {
                    //Thêm các thuộc tính vào session
                    Session["TypeOfUser"] = lg.TypeOfUser_ID.ToString();
                    Session["UserName"] = lg.User_Name.ToString();
                    Session["Name"] = lg.User_Account.ToString();
                    Session["id"] = lg.User_ID.ToString();
                    //lấy danh sách giỏ hàng 
                    var listCart = entities.Carts.ToList();
                    List<Cart> ifAvailable = new List<Cart>();
                    double User_id = Convert.ToDouble(Session["id"]); // lấy userID từ session
                    Cart cart = new Cart();
                    //dòng for để check xem ngdùng có giỏ hàng hay chưa , nếu có để nó vào 1 list là ifAvailable
                    for (int i = 0; i < listCart.Count; i++)
                    {
                        var item = listCart[i];
                        if (item.User_ID == User_id)
                        {
                            ifAvailable.Add(item);
                        }
                    }
                    if (ifAvailable.Count == 0)// nếu như không có giỏ hàng tạo giỏ hàng mới 
                    {
                        entities.Carts.Add(cart);
                        cart.Cart_ID = listCart.Count + 1;// tổng giỏ hàng + 1
                        cart.User_ID = User_id;
                        Debug.WriteLine("tao gio hang" + cart.Cart_ID + "\n\n\n");
                        Session["cartid"] = cart.Cart_ID;// gán giá trị cartID vào session
                        entities.SaveChanges();// lưu vào database
                    }
                    else if (ifAvailable.Count != 0)// nếu như có sẳn 
                    {
                        for (int i = 0; i < ifAvailable.Count; i++)//dòng for để chạy hết tất cả giỏ hàng mà ngdùng sở hữu 
                        {
                            if (ifAvailable.Count == i + 1)// nếu biến i trùng gtrị tối đa với giỏ hàng mà ngdùng sở hữu
                            {
                                Session["cartid"] = ifAvailable[i].Cart_ID;// gág session
                            }
                        }
                    }

                    return RedirectToAction("Index", "Home");
                }
                else if (lg != null && lg.TypeOfUser_ID == 3)// account bị xoá
                {
                    ViewBag.Error = "This account is delete!!!";
                }
                else
                {
                    // in thông báo lỗi nếu như tài khoản hoặc pass không đúng
                    ViewBag.Error = "Username or password not correct";
                }
            }
            return View();
        }
        public ActionResult Logout()
        {
            // xoá hết các session đang có 
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        // GET: Login/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Login/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Login/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Login/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
