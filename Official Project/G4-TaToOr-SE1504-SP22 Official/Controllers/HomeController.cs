﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class HomeController : Controller
    {
        TaToOrEntities context = new TaToOrEntities();
        public ActionResult Index()
        {
            double id = Convert.ToInt32(Session["TypeOfUser"]);
            ViewBag.listTOU = context.TypeOfUsers.Find(id);
            ViewBag.P = context.Pictures.ToList();
            DateTime b = DateTime.Now;
            var sxngay = context.Bills.OrderBy(s => s.Bill_Date).ToList(); // Gán giá trị ngày bắt đầu và ngày kết thúc
            DateTime a = new DateTime();
            if (sxngay.Count > 0)
            {
                a = sxngay[0].Bill_Date;
            }
            ViewBag.a = a;
            ViewBag.b = b;

            var listBillAvailable = context.Bills.Where(model => model.Bill_Date >= a && model.Bill_Date <= b).ToList(); // dùng LinQ lấy giá trị giữa ngày bắt đầu và ngày kết thúc
            var listDetailBillAvailable = new List<BillDetail>(); // tạo 1 danh sách chứa các chi tiết của từng đơn
            for (int i = 0; i < listBillAvailable.Count; i++)
            {
                double billID = listBillAvailable[i].Bill_ID;
                //Danh sách của từng thành phần chi tiết trong đơn hàng chi tiết ( ví dụ Bill ID là 1 có 3 cart detail thì danh sách ngày có 3 phần tử )
                var listNumberBillDetailAvailable = context.BillDetails.Where(model => model.Bill_ID == billID && model.Bill.BillStatus_ID == 2).ToList();
                for (int j = 0; j < listNumberBillDetailAvailable.Count; j++)
                {
                    //thêm từng sản phẩm vào list đó.
                    listDetailBillAvailable.Add(listNumberBillDetailAvailable[j]);
                }
            }
            ViewBag.list = listDetailBillAvailable;

            //foreach (var listBS in listDetailBillAvailable.GroupBy(info => info.CartDetail.Product.Product_Name)
            //            .Select(group => new {
            //                Metric = group.Key,
            //                Count = group.Count()
            //            })
            //            .OrderBy(x => x.Metric))
            for (int i = 0; i < listDetailBillAvailable.Count; i++)
            {

                double Product_IDiList = listDetailBillAvailable[i].CartDetail.Product.Product_ID;
                double Product_Quantity = (double)listDetailBillAvailable[i].CartDetail.CartDetail_QuantityProduct;
                double Product_TotalMoneyOfProduct = (double)listDetailBillAvailable[i].CartDetail.CartDetail_TotalOfProduct;
                for (int j = 0; j < i; j++)
                {
                    // vòng lặp kiểm tra xem có món ăn trùng nhau ko, nếu có tự động vào

                    double Product_IDjList = listDetailBillAvailable[j].CartDetail.Product.Product_ID;
                    if (Product_IDiList == Product_IDjList)
                    {
                        listDetailBillAvailable[i].CartDetail.CartDetail_QuantityProduct += listDetailBillAvailable[j].CartDetail.CartDetail_QuantityProduct;
                        listDetailBillAvailable[i].CartDetail.CartDetail_TotalOfProduct += listDetailBillAvailable[j].CartDetail.CartDetail_TotalOfProduct;
                    }
                }

            }

            listDetailBillAvailable = listDetailBillAvailable.GroupBy(model => new { model.CartDetail.Product.Product_ID, model.CartDetail.Product.Product_Name}).Select(model => model.First()).ToList();
            listDetailBillAvailable = listDetailBillAvailable.OrderBy(model => model.CartDetail.CartDetail_QuantityProduct).Reverse().Take(4).ToList(); // sort theo số lượng 



            ViewBag.listDetailBillAvailable = listDetailBillAvailable;


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult SOS()
        {
            return View();
        }
    }
}