﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class CartController : Controller
    {
        // GET: AddToCart
        TaToOrEntities entities = new TaToOrEntities();

        public ActionResult Cart(int id)
        {
            Cart cart = new Cart();
            CartDetail cartDetail = new CartDetail();
            int user_id = 0;
            double iddetail = 0;
            
            user_id = Convert.ToInt32(Session["id"]);
            if (user_id == 0)
            {
                RedirectToAction("Login", "Login");
            }

            else
            {
                double CartID = Convert.ToDouble(Session["cartid"]);
                cart.Cart_ID = CartID;
                var listCartDetail = entities.CartDetails.ToList();
                int listCount = listCartDetail.Count;
                List<CartDetail> availablecart = new List<CartDetail>();
                var getProduct = entities.Products.Find(id);
                float getPriceProduct = (float)getProduct.Product_Price;
                if (listCount > 0)
                {
                    for (int m = 0; m < listCount; m++)
                    {
                        var product = listCartDetail[m];

                        if (product.Cart_ID == cart.Cart_ID && product.Product_ID == id)
                        {
                            var getCartDetail = entities.CartDetails.Find(product.CartDetail_ID);

                            getCartDetail.CartDetail_QuantityProduct += 1;

                            getCartDetail.CartDetail_TotalOfProduct = (getPriceProduct * getCartDetail.CartDetail_QuantityProduct);
                            availablecart.Add(product);

                        }
                        else if (product.Product_ID != id)
                        {
                            availablecart.Add(product);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < listCount; i++)
                    {

                        if (listCartDetail[i].CartDetail_ID == i + 1)
                        {

                            iddetail = listCartDetail[i].CartDetail_ID;
                        }
                    }
                    entities.CartDetails.Add(cartDetail);
                    cartDetail.CartDetail_ID = iddetail+1;
                    cartDetail.Cart_ID = CartID;
                    cartDetail.Product_ID = id;
                    cartDetail.CartDetail_QuantityProduct = 1;

                }
                int countsp = 0;
                for (int i = 0; i < availablecart.Count; i++)
                {
                    var n = availablecart[i];
                    if (n.Product_ID == id)
                    {
                        countsp += 1;
                    }
                }
                if (countsp == 0)
                {
                    for (int i = 0; i < listCount; i++)
                    {

                        if (listCartDetail[i].CartDetail_ID == i + 1)
                        {

                            iddetail = listCartDetail[i].CartDetail_ID;
                        }
                    }
                    entities.CartDetails.Add(cartDetail);
                    cartDetail.CartDetail_ID = iddetail + 1;
                    cartDetail.Cart_ID = CartID;
                    cartDetail.Product_ID = id;
                    cartDetail.CartDetail_QuantityProduct = 1;
                    cartDetail.CartDetail_TotalOfProduct = 1 * getPriceProduct;
                    entities.SaveChanges();

                }
                entities.SaveChanges();
            }

            //int SoLuong = 1;
            //cartDetail.CartDetails_QuantityProduct = SoLuong;
            //entities.CartDetails.Add(cartDetail
            //entities.SaveChanges();


            return RedirectToAction("ShowProducts", "Products");
        }
        public ActionResult AddToCartProductDetail(int id)
        {
            int NumProduct = Convert.ToInt32(Request["num-product"]);
            Session["num"] = Convert.ToString(NumProduct);

            
            Cart cart = new Cart();
            CartDetail cartDetail = new CartDetail();
            double user_id = Convert.ToDouble(Session["id"]);
            if (user_id == 0)
            {
                return RedirectToAction("Login", "Login");
            }

            else
            {

                double CartID = Convert.ToDouble(Session["cartid"]);
                cart.Cart_ID = CartID;
                var listCartDetail = entities.CartDetails.ToList();
                int listCount = listCartDetail.Count;
                List<CartDetail> availablecart = new List<CartDetail>();
                var getProduct = entities.Products.Find(id);
                float getPriceProduct = (float)getProduct.Product_Price;
                if (listCount > 0)
                {
                    for (int m = 0; m < listCount; m++)
                    {
                        var product = listCartDetail[m];

                        if (product.Cart_ID == cart.Cart_ID && product.Product_ID == id)
                        {
                            var getCartDetail = entities.CartDetails.Find(product.CartDetail_ID);

                            getCartDetail.CartDetail_QuantityProduct += NumProduct;

                            getCartDetail.CartDetail_TotalOfProduct = (getPriceProduct * getCartDetail.CartDetail_QuantityProduct);
                            availablecart.Add(product);

                        }
                        else if (product.Product_ID != id)
                        {
                            availablecart.Add(product);
                        }
                    }
                }
                else
                {
                    double iddetail = 0;
                    for (int i = 0; i < listCount; i++)
                    {

                        if (listCartDetail[i].CartDetail_ID == i + 1)
                        {

                            iddetail = listCartDetail[i].CartDetail_ID;
                        }
                    }
                    entities.CartDetails.Add(cartDetail);
                    cartDetail.CartDetail_ID = iddetail+1;
                    cartDetail.Cart_ID = CartID;
                    cartDetail.Product_ID = id;
                    cartDetail.CartDetail_QuantityProduct = NumProduct;
                    cartDetail.CartDetail_TotalOfProduct = NumProduct * getPriceProduct;

                }
                int countsp = 0;
                for (int i = 0; i < availablecart.Count; i++)
                {
                    var n = availablecart[i];
                    if (n.Product_ID == id)
                    {
                        countsp += 1;
                    }
                }
                if (countsp == 0)
                {
                    double iddetail = 0;
                    for (int i = 0; i < listCount; i++)
                    {

                        if (listCartDetail[i].CartDetail_ID == i + 1)
                        {

                            iddetail = listCartDetail[i].CartDetail_ID;
                        }
                    }
                    entities.CartDetails.Add(cartDetail);
                    cartDetail.CartDetail_ID = iddetail + 1;
                    cartDetail.Cart_ID = CartID;
                    cartDetail.Product_ID = id;
                    cartDetail.CartDetail_QuantityProduct = NumProduct;
                    cartDetail.CartDetail_TotalOfProduct = NumProduct * getPriceProduct;
                    entities.SaveChanges();
                }
                entities.SaveChanges();
            }

            //int SoLuong = 1;
            //cartDetail.CartDetails_QuantityProduct = SoLuong;
            //entities.CartDetails.Add(cartDetail);


            return RedirectToAction("ShowProducts", "Products");
        }

        // GET: AddToCart/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AddToCart/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AddToCart/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: AddToCart/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AddToCart/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AddToCart/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

