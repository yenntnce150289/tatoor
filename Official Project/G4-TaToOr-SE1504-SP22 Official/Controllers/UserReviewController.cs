﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class UserReviewController : Controller
    {
        TaToOrEntities context = new TaToOrEntities();
        // GET: UserReview
        public ActionResult Index()
        {
            return View();
        }

        // GET: UserReview/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserReview/Create
        // POST: UserReview/Create
        [HttpPost]
        public ActionResult CreateReview(ReviewFile reviewcome)
        {
            Review review = new Review();

            try
            {
                review.Review_ID = (context.Reviews.Count()) + 1;
                review.Review_Date = DateTime.Now;
                review.ReviewShow_ID = 0;
                review.ReviewEdit_ID = 1;
                review.User_ID = Convert.ToDouble(Session["id"]);
                if (reviewcome.Review_Rate == 0)
                {
                    review.Review_Rate = 5;
                }
                else
                {
                    review.Review_Rate = reviewcome.Review_Rate;
                }
                review.Review_Comment = reviewcome.Review_Comment;
                review.Product_ID = reviewcome.Product_ID;

                for (int step = 1; step < 3; step++)
                {
                    if (step == 1)
                    {
                        context.Reviews.Add(review);
                        context.SaveChanges();

                    }
                    else if (step == 2)
                    {
                        for (int i = 0; i < reviewcome.files.Count; i++)
                        {
                            var file = reviewcome.files[i];
                            if (file != null && file.ContentLength > 0)
                            {
                                PictureReview pictureReview = new PictureReview();
                                pictureReview.Review_ID = review.Review_ID;
                                pictureReview.PictureReview_ID = context.PictureReviews.ToList().Count + 1;
                                string pictureName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                                pictureReview.Picture_Link = "/img/pictureReview/"+ pictureName;
                                file.SaveAs(Path.Combine(Server.MapPath("/img/pictureReview/"), pictureName));
                                context.PictureReviews.Add(pictureReview);
                                context.SaveChanges();
                            }
                        }
                    }
                }

                // TODO: Add insert logic here


                return RedirectToAction("Productdetail/" + review.Product_ID, "Products");


            }
            catch
            {
                return View();
            }
        }

        // GET: UserReview/Edit/5
        public ActionResult EditReview(int id)
        {
            return View();
        }

        // POST: UserReview/Edit/5
        [HttpPost]
        public ActionResult EditReview(Review review)
        {
            try
            {
                var listreview = context.Reviews.ToList();
                for (int i = 0; i < listreview.Count; i++)
                {
                    if (listreview[i].Review_ID == review.Review_ID)
                    {
                        var oldreview = listreview[i];
                        oldreview.Review_Rate = review.Review_Rate;
                        oldreview.Review_Comment = review.Review_Comment;
                        oldreview.ReviewShow_ID = 0;
                        oldreview.ReviewEdit_ID = 0;
                        oldreview.Review_Date = DateTime.Now;
                        context.SaveChanges();
                    }
                }

                // TODO: Add insert logic here

                return RedirectToAction("Productdetail/" + review.Product_ID, "Products");
            }
            catch
            {
                return View();
            }
        }

        // GET: UserReview/Delete/5
        public ActionResult Delete123(int id)
        {
            return View();
        }

        // POST: UserReview/Delete/5
        [HttpPost]
        public ActionResult Delete123(int id, FormCollection collection)
        {
            try
            {
                //Yen Ngoc Ngech
                //CkiNguyen lam phan nay ne
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
