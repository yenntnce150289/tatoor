﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class OrderHistoryController : Controller
    {
        // GET: OrderHistory
        TaToOrEntities entities = new TaToOrEntities();


        public ActionResult OrderHistory()
        {
            int User_id = 0;
            User_id = Convert.ToInt32(Session["id"]);// lấy user ID trên session
            List<Bill> listBi = new List<Bill>();// tạo list để chưa bill của user đó
            if (User_id == 0)// bắt lỗi chưa đăng nhập
            {
                return RedirectToAction("NoAcc", "OrderHistory");// chuyển đến trang lỗi
            }
            else
            {

                List<Cart> ListCart = entities.Carts.Where(a => a.User_ID == User_id).ToList();// tìm các cart của User đó
                List<Bill> ListBill = entities.Bills.ToList();// lấy danh sách Bill
                foreach (var item in ListBill)// check list bill
                {
                    foreach (var a in ListCart)// check list cart
                    {
                        if (item.Cart_ID == a.Cart_ID)// check xem cart có trong bill không
                        {
                            listBi.Add(item);// thêm nó vào list
                        }
                    }
                }
                if (listBi.Count == 0)// check nếu chưa mua hàng
                {
                    ViewBag.Nul = "You haven't bill";
                }
            }

            return View(listBi);
        }
        public ActionResult ViewDetail(int id)
        {
            ViewBag.checkBill = entities.Bills.Find(id);// tìm bill theo cartID
            ViewBag.CheckBillDetail = entities.BillDetails.Where(a => a.Bill_ID == id).ToList();//tìm bill detail bằng bill id
            ViewBag.checkCartDetail = entities.CartDetails.ToList();//lấy danh sách Cartdetail để lấy product 
            return View();
        }
        public ActionResult NoAcc()// trang thông báo lỗi
        {

            return View();
        }
        // GET: OrderHistory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: OrderHistory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderHistory/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderHistory/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OrderHistory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderHistory/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: OrderHistory/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
