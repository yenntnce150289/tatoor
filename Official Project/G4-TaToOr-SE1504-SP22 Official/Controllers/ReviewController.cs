﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G4_TaToOr_SE1504_SP22_Official.Models;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class ReviewController : Controller
    {
        TaToOrEntities entities = new TaToOrEntities();
        // GET: Review
        public ActionResult ReviewIndex()
        {
            var list = entities.Reviews.ToList();
            ViewBag.All = entities.Reviews.Count();
            double id = Convert.ToInt32(Session["TypeOfUser"]);
            ViewBag.Id = id;
            return View(list);
        }
      // GET: Review/Edit/5
        public ActionResult ReviewEdit(int id)
        {
            double type = Convert.ToInt32(Session["TypeOfUser"]);
            ViewBag.Id = type;
            var context = new TaToOrEntities();
            var editing = context.Reviews.Find(id);
            return View(editing);
        }

        // POST: Review/Edit/5
        [HttpPost]
        public ActionResult ReviewEdit(Review model)
        {
            try
            {
                // TODO: Add update logic here

                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                var context = new TaToOrEntities();
                var editing = context.Reviews.Find(model.Review_ID);
                editing.ReviewShow_ID = model.ReviewShow_ID;
                context.SaveChanges();
                return RedirectToAction("ReviewIndex");
            }
            catch
            {
                return View();
            }
        }
    }
}
