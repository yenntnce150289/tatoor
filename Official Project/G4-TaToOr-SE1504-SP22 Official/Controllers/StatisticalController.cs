﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class StatisticalController : Controller
    {
        TaToOrEntities context = new TaToOrEntities();
        // GET: Statistical
        public ActionResult Index()
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }
        }
        [HttpPost]
        public ActionResult Index(Statistical a)
        {
            double type = Convert.ToInt32(Session["TypeOfUser"]);
            ViewBag.Id = type;
            if (a.value1 <= a.value2) // điều kiện nếu giá trị ngày kết thúc ( value 2 ) nhỏ hơn ngày bắt đầu (value 1) thì chạy tập lệnh dướ đây
            {
                DateTime ngaybatdau = a.value1; // Gán giá trị ngày bắt đầu và ngày kết thúc
                DateTime ngayketthuc = a.value2;
                //danh sách lấy ngày hợp lệ giữa ngày bắt đầu và kết thúc
                var listBillAvailable = context.Bills.Where(model => model.Bill_Date >= ngaybatdau && model.Bill_Date <= ngayketthuc).ToList(); // dùng LinQ lấy giá trị giữa ngày bắt đầu và ngày kết thúc
                var listDetailBillAvailable = new List<BillDetail>(); // tạo 1 danh sách chứa các chi tiết của từng đơn thành công
                var listBoom = new List<BillDetail>(); // tạo 1 danh sách chứa các chi tiết của từng đơn bị huỷ
                var listStatisticalDetail = new List<StatisticalDetail>();
                var listBoomDetail = new List<DetailBoom>();
                for (int i = 0; i < listBillAvailable.Count; i++)
                {
                    double billID = listBillAvailable[i].Bill_ID;
                    //Danh sách của từng thành phần chi tiết trong đơn hàng chi tiết ( ví dụ Bill ID là 1 có 3 cart detail thì danh sách ngày có 3 phần tử )
                    var listNumberBillDetailAvailable = context.BillDetails.Where(model => model.Bill_ID == billID && model.Bill.BillStatus_ID == 2).ToList(); // BillStatus_ID=2 nghĩa là đơn giao thành công mới tính
                    for (int j = 0; j < listNumberBillDetailAvailable.Count; j++)
                    {
                        //thêm từng sản phẩm vào list đó.
                        listDetailBillAvailable.Add(listNumberBillDetailAvailable[j]);
                    }
                    var listNumberBoom = context.BillDetails.Where(model => model.Bill_ID == billID && model.Bill.BillStatus_ID == 4).ToList(); // BillStatus_ID=4 nghĩa là đơn bị Boom hàng
                    for (int j = 0; j < listNumberBoom.Count; j++)
                    {
                        //thêm từng sản phẩm vào list đó.
                        listBoom.Add(listNumberBoom[j]);
                    }
                }


                ViewBag.listdetailbill = listDetailBillAvailable;
                ViewBag.BillSucessCount = listBillAvailable.Where(model => model.BillStatus_ID == 2).ToList().Count;
                ViewBag.BillFailCount= listBillAvailable.Where(model => model.BillStatus_ID == 4).ToList().Count;
                ViewBag.listBillAvailable = listBillAvailable;


                for (int i = 0; i < listDetailBillAvailable.Count; i++)
                {
                    double Product_ID_Bestseller = listDetailBillAvailable[i].CartDetail.Product.Product_ID;
                    double Product_IDiList = listDetailBillAvailable[i].CartDetail.Product.Product_ID;
                    string Product_Name = listDetailBillAvailable[i].CartDetail.Product.Product_Name.Trim();
                    double Product_Quantity = (double)listDetailBillAvailable[i].CartDetail.CartDetail_QuantityProduct;
                    double Product_TotalMoneyOfProduct = (double)listDetailBillAvailable[i].CartDetail.CartDetail_TotalOfProduct;
                    for (int j = 0; j < i; j++)
                    {
                        // vòng lặp kiểm tra xem có món ăn trùng nhau ko, nếu có tự động vào
                        double Product_IDjList = listDetailBillAvailable[j].CartDetail.Product.Product_ID;
                        if (Product_IDiList == Product_IDjList)
                        {
                            Product_Quantity += (double)listDetailBillAvailable[j].CartDetail.CartDetail_QuantityProduct;
                            Product_TotalMoneyOfProduct += (double)listDetailBillAvailable[j].CartDetail.CartDetail_TotalOfProduct;
                        }
                    }

                    if (listStatisticalDetail.Count == 0)
                    {
                        StatisticalDetail n = new StatisticalDetail();
                        n.Product_ID = Product_ID_Bestseller;
                        n.Product_Name = Product_Name;
                        n.Product_Quantity = Product_Quantity;
                        n.Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                        listStatisticalDetail.Add(n);
                    }
                    else
                    {
                        StatisticalDetail n = new StatisticalDetail();
                        n.Product_ID = Product_ID_Bestseller;
                        n.Product_Name = Product_Name;
                        n.Product_Quantity = Product_Quantity;
                        n.Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                        for (int k = 0; k < listStatisticalDetail.Count; k++)
                        {
                            double availablecount = 0;
                            if ((listStatisticalDetail[k].Product_ID== Product_IDiList) == true)
                            {
                                listStatisticalDetail[k].Product_Quantity = Product_Quantity;
                                listStatisticalDetail[k].Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                            }
                            else if ((listStatisticalDetail[k].Product_ID == Product_IDiList)== false)
                            {
                                availablecount += 1;
                            }
                            if (availablecount == k + 1)
                            {
                                listStatisticalDetail.Add(n);
                            }
                        }
                    }
                }

                //Vòng lặp chạy danh sách Boom hàng 
                for (int i = 0; i < listBoom.Count; i++)
                {
                    double User_IDiList = listBoom[i].CartDetail.Cart.User_ID;
                    double Product_IDiList = listBoom[i].CartDetail.Product.Product_ID;
                    string ProductNameiList = listBoom[i].CartDetail.Product.Product_Name.Trim();
                    string Product_Name = listBoom[i].CartDetail.Product.Product_Name.Trim();
                    double Product_Quantity = (double)listBoom[i].CartDetail.CartDetail_QuantityProduct;
                    double Product_TotalMoneyOfProduct = (double)listBoom[i].CartDetail.CartDetail_TotalOfProduct;
                    for (int j = 0; j < i; j++)
                    {
                        // vòng lặp kiểm tra xem có món ăn trùng nhau ko, nếu có tự động vào
                        string ProductNamejList = listBoom[j].CartDetail.Product.Product_Name.Trim();
                        double User_IDjList = listBoom[j].CartDetail.Cart.User_ID;
                        double Product_IDjList = listBoom[j].CartDetail.Product.Product_ID;
                        if (Product_IDiList == Product_IDjList && User_IDiList == User_IDjList)
                        {
                            Product_Name = ProductNameiList;
                            Product_Quantity += (double)listBoom[j].CartDetail.CartDetail_QuantityProduct;
                            Product_TotalMoneyOfProduct += (double)listBoom[j].CartDetail.CartDetail_TotalOfProduct;
                        }
                    }
                    DetailBoom n = new DetailBoom();
                    n.User_ID = User_IDiList;
                    n.Product_ID = Product_IDiList;
                    n.Product_Quantity = Product_Quantity;
                    n.Product_TotalMoneyOfProduct = Product_TotalMoneyOfProduct;
                    n.Product_Name = Product_Name;
                    listBoomDetail.Add(n);
                }



                //Sản phẩm bán nhiều nhất: 
                listStatisticalDetail = listStatisticalDetail.GroupBy(model => new { model.Product_ID, model.Product_Name, model.Product_Quantity, model.Product_TotalMoneyOfProduct }).Select(model => model.First()).ToList(); // gộp bảng các phần tử trùng
                listStatisticalDetail = listStatisticalDetail.OrderBy(model => model.Product_Quantity).Reverse().ToList(); // sort theo số lượng
                double BestSeller_Quantity = 0;
                string Product_Name_BestSeller = "";
                var listimageproduct = new List<Picture>();
                string imageproductlink = "";
                double Product_ID = 0;
                if (listStatisticalDetail.Count != 0)
                {
                    BestSeller_Quantity = listStatisticalDetail[0].Product_Quantity;
                    Product_Name_BestSeller = listStatisticalDetail[0].Product_Name;
                    Product_ID = listStatisticalDetail[0].Product_ID;
                    listimageproduct = context.Pictures.Where(model => model.Product_ID == Product_ID).ToList();
                    if (listimageproduct.Count > 0)
                    {
                        imageproductlink = listimageproduct[0].Picture_Link;
                    }
                }
                double Revenue = 0;
                for (int i = 0; i < listStatisticalDetail.Count; i++)
                {
                    Revenue += listStatisticalDetail[i].Product_TotalMoneyOfProduct;
                }

                listBoomDetail = listBoomDetail.GroupBy(model => new { model.User_ID, model.Product_ID, model.Product_Name, model.Product_Quantity, model.Product_TotalMoneyOfProduct }).Select(model => model.First()).ToList(); // gộp bảng các phần tử trùng
                listBoomDetail = listBoomDetail.OrderBy(model => model.Product_TotalMoneyOfProduct).Reverse().ToList(); // sort theo giá tiền
                
                ViewBag.Detail = listStatisticalDetail;
                ViewBag.Quantity = BestSeller_Quantity;
                ViewBag.Product_Name = Product_Name_BestSeller;
                ViewBag.value1 = a.value1.ToString("yyyy-MM-dd");
                ViewBag.value2 = a.value2.ToString("yyyy-MM-dd");
                ViewBag.Revenue = Revenue;
                ViewBag.imageproductlink = imageproductlink;
                ViewBag.Product_ID = Product_ID;
                ViewBag.listBoomDetail = listBoomDetail;
            }
            else
            {

            } 
            return View();
        }
        // GET: Statistical/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Statistical/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Statistical/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Statistical/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Statistical/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Statistical/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Statistical/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
