﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class BillController : Controller
    {
        TaToOrEntities entities = new TaToOrEntities();
        // GET: Bill
        public ActionResult ShowBill(string message)
        {

            ViewBag.MessageBill = message;// truyền biến thông báo tạo bill
            //ViewBag.shipping = "None";
            double CartID = Convert.ToDouble(Session["cartid"]);
            ViewBag.showcart = entities.CartDetails.Where(a => a.Cart_ID == CartID).ToList();//get list cartdetail
            ViewBag.pictrureProduct = entities.Pictures.ToList();


            ViewBag.totalBill = 0; //tạo giá trị ban đầu cho Tổng bill
            ViewBag.Img = entities.Pictures.ToList();// tạo list Pictrures
            ViewBag.QuantityProduct = 0;//tạo giá trị ban đầu 

            //vòng lặp tính totalbill và numberofproduct trong bill
            for (int i = 0; i < ViewBag.showcart.Count; i++)
            {
                ViewBag.totalBill = ViewBag.totalBill + ViewBag.showcart[i].CartDetail_TotalOfProduct;// sum total bill
                ViewBag.QuantityProduct = ViewBag.QuantityProduct + ViewBag.showcart[i].CartDetail_QuantityProduct;

            }
            //int User_id = 0;
            //User_id = Convert.ToInt32(Session["id"]);
            //if ViewBag.totalBill > 300 then ViewBag.totalBill +15000
            if (ViewBag.totalBill > 300000 || ViewBag.totalBill == 0)
            {
                ViewBag.shipping = "Free Ship";
                ViewBag.totalBillAfterShip = ViewBag.totalBill;
            }
            else
            {
                ViewBag.shipping = "Ship 15000 VNĐ";
                ViewBag.totalBillAfterShip = ViewBag.totalBill + 15000;//+15000vnd shipping
            }


            int User_id = 0;
            User_id = Convert.ToInt32(Session["id"]);
            User user = new User();
            
            //tạo list user
            var listUser = entities.Users.Where(a => a.User_ID == User_id).ToList();

            for (int i = 0; i < listUser.Count; i++)
            {
                if (listUser[i].User_ID == User_id)
                {
                    user = listUser[i];
                }
            }
            ViewBag.infor = user;// lấy thông tin user 


            return View();
        }

        // GET: Bill/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        // GET: Bill/Create
        public ActionResult CreateBill(Bill bill)
        {


            double User_id = Convert.ToDouble(Session["id"]);// lấy userID từ session
            double SSCartID = Convert.ToDouble(Session["cartid"]);
            //line 85 - 88 lấy các giá trị tương ứng từ khung đăng nhập
            string address = Request["address"];
            string phone = Request["phonenumber"];
            string note = Request["note"];
            string getnumberProduct = Request["numberproduct"];
            string getbilltotal = Request["totalBill"];
            string getpayment = Request["methodpayment"];


            //chuyển giá trị tương ứng string => int  và double
            int numberofproduct = Convert.ToInt32(getnumberProduct);
            double TotalBill = Convert.ToDouble(getbilltotal);
            //xét bill rỗng thì không cho tạo bill và gửi message
            if (numberofproduct == 0)
            {
                TotalBill = 0;
                return RedirectToAction("ShowBill", "Bill", new { message = "Please enter information!" });

            }

            for (int step = 1; step < 4; step++) // có 3 bước: 
            {
                if (step == 1) // Bước 1 tạo 1 bill trước
                {
                   
                    var listbill = entities.Bills.ToList();//tạo listbill để lấy bill_ID +1
                    var listCartDetail = entities.CartDetails.ToList();
                    var listBillStatasId = entities.Bills.Where(a => a.BillStatus_ID == 4).ToList();//list bill boom
                    double count = 0; // count số lần boom hàng
                    for (int i = 0; i < listBillStatasId.Count; i++)
                    {
                        double id_user = listBillStatasId[i].Cart.User_ID;
                        if (id_user == User_id)
                        {
                            count++;
                        }
                    }
                    if (count <= 2) // nếu boom hàng dưới 2 lần thì cho phép tạo bill
                    {
                        // gáng giá trị Bills vào database
                        bill.Bill_ID = listbill.Count + 1;
                        bill.Cart_ID = SSCartID;
                        bill.Bill_Address = address;
                        bill.Bill_Total = TotalBill;
                        bill.Bill_Date = DateTime.Now;
                        bill.Bill_PaymentMethods = getpayment;
                        bill.BillStatus_ID = 0;
                        bill.Bill_QuantityProduct = numberofproduct;
                        bill.Bill_NoteProduct = phone + "  " + note;// them so dien thoai vao dong note

                        entities.Bills.Add(bill);//add Bill vừa gáng giá trị
                    }
                    else if (count > 2) // boom hàng 3 lần thông báo
                    {
                        ViewBag.Alert("Bùm hàng chúa mà đòi mua hàng hả :<");
                    }
                }
                else if (step == 2) // Bước 2: Thêm nó nào billdetails nè 
                {
                    var getCartDetailValue = entities.CartDetails.Where(a => a.Cart_ID == SSCartID).ToList();
                    for (int i = 0; i < getCartDetailValue.Count; i++)
                    {
                        var cartDetail = getCartDetailValue[i];

                        BillDetail billDetail = new BillDetail();
                        billDetail.BillDetail_ID = (entities.BillDetails.ToList().Count) + 1;
                        Debug.WriteLine((entities.BillDetails.ToList().Count) + 1);
                        billDetail.Bill_ID = bill.Bill_ID;
                        billDetail.CartDetail_ID = cartDetail.CartDetail_ID;
                        entities.BillDetails.Add(billDetail);
                        entities.SaveChanges();
                        
                    }
                }
                else if (step == 3) // Bước 3 tạo cart id mới cho đơn hàng.
                {
                    //lấy danh sách giỏ hàng 
                    var newcart = new Cart();
                    newcart.Cart_ID= entities.Carts.ToList().Count+1;
                    newcart.User_ID = User_id;
                    entities.Carts.Add(newcart);
                    entities.SaveChanges();
                    Session["cartid"] = newcart.Cart_ID;
                    //dòng for để check xem ngdùng có giỏ hàng hay chưa , nếu có để nó vào 1 list là ifAvailable


                }
            }
            return RedirectToAction("CreateBillFinished", "Bill", new { message = "Create Bill Successfully!" });


        }

        // GET: Bill/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Bill/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Bill/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Bill/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult CreateBillFinished()
        {
            return View();
        }
    }
}