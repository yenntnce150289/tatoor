﻿using G4_TaToOr_SE1504_SP22_Official.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class ProductsController : Controller
    {
        TaToOrEntities entities = new TaToOrEntities();
        // GET: Products
        public ActionResult ShowProducts(int origin = 0, int option = 0, int form = 0)
        {
            // lấy cartid từ session
            double cartid = Convert.ToDouble(Session["cartid"]);
            //lấy danh sách sản phẩm
            //ViewBag.pro = entities.Products.ToList();
            // lấy danh sách hình ảnh sản phẩm
            ViewBag.pic = entities.Pictures.ToList();
            // thêm các giá trị của cartid tương ứng vào viewbag
            ViewBag.yourcart = entities.CartDetails.Where(a => a.Cart_ID == cartid).ToList();
            List<Product> ListProducts = new List<Product>();// tạo 1 list để chứa product

            // filter sản phẩm
            //tạo biến cho biến
            ViewBag.Origin = origin;// đất nước
            ViewBag.option = option;// giá tiền
            ViewBag.Form = form;// dạng thức ăn

            //thêm danh sách hiển thị của giá 
            List<String> ListPrice = new List<String>();
            ListPrice.Add("0-50.000");
            ListPrice.Add("50.000-100.000");
            ListPrice.Add("100.000-200.000");
            ListPrice.Add(">200.000");
            ViewBag.ListPrice = ListPrice; // thêm nó vào viewbag
                                           //Thêm một danh sách đất nước
            List<Origin> ListOrigin = entities.Origins.ToList();
            ViewBag.ListOri = ListOrigin;// thêm nó vào viewbag
            List<Product> listPro = new List<Product>();
            //thêm danh sách định dạng 
            List<FormOfFood> Listforms = entities.FormOfFoods.ToList();
            ViewBag.Listforms = Listforms; // thêm nó vào viewbag
                                           //kiểm tra danh sách 
            if (origin == 0 && form == 0) //chưa có filter
            {
                ListProducts = entities.Products.ToList();
            }
            else if (form == 0)// không có định dạng nhưng có đất nước
            {
                ListProducts = entities.Products.Where(a => a.Origin_ID == origin).ToList();
            }
            else if (origin == 0)// không có đất nước nhưng có định dạng
            {
                ListProducts = entities.Products.Where(a => a.FormOfFood_ID == form).ToList();
            }
            else // có tất cả trường hợp
            {
                ListProducts = entities.Products.Where(a => a.Origin_ID == origin && a.FormOfFood_ID == form).ToList();

            }
            // các trường hợp của giá tiền
            if (option == 0)
            {
                ViewBag.pro = ListProducts;
            }
            else if (option == 1)
            {
                List<Product> List1 = ListProducts.Where(i => i.Product_Price <= 50000).ToList();
                ViewBag.pro = List1;

                // ViewBag.pro = entities.Products.Where(a => a.Product_Price > 0 && a.Product_Price < 50000).ToList();
            }
            else if (option == 2)
            {
                List<Product> List2 = ListProducts.Where(a => a.Product_Price > 50000 && a.Product_Price <= 100000).ToList();
                ViewBag.pro = List2;


                //ViewBag.pro = entities.Products.Where(a => a.Product_Price > 50000 && a.Product_Price < 100000).ToList();
            }
            else if (option == 3)
            {
                List<Product> List3 = ListProducts.Where(a => a.Product_Price > 100000 && a.Product_Price < 200000).ToList();
                ViewBag.pro = List3;

                // ViewBag.pro = entities.Products.Where(a => a.Product_Price > 100000 && a.Product_Price < 200000).ToList();
            }
            else if (option == 4)
            {
                List<Product> List4 = ListProducts.Where(a => a.Product_Price >= 200000).ToList();
                ViewBag.pro = List4;
                //ViewBag.pro = entities.Products.Where(a => a.Product_Price >= 200000).ToList();
            }
            if (ViewBag.pro.Count == 0)
            {
                ViewBag.Nul = "No product ";
            }
            return View();
        }
        public ActionResult Productdetail(double id)
        {
            double cartid = Convert.ToDouble(Session["cartid"]);
            //lấy danh sách sản phẩm
            //ViewBag.pro = entities.Products.ToList();
            // lấy danh sách hình ảnh sản phẩm
            ViewBag.PicYourCart = entities.Pictures.ToList();
            // thêm các giá trị của cartid tương ứng vào viewbag
            ViewBag.yourcart = entities.CartDetails.Where(a => a.Cart_ID == cartid).ToList();
            List<Product> ListProducts = new List<Product>();// tạo 1 list để chứa product
            //int count = ViewBag.yourcart.Count;
            //Start===Show review===
            ViewBag.RvPD = entities.Reviews.Where(a => a.Product_ID == id && a.ReviewShow_ID == 1).ToList(); //get list review by Product_id
            ViewBag.PicRV = entities.PictureReviews.ToList();

            //tinh Star avg
            double sumRate = 0;
            for (int i = 0; i < ViewBag.RvPD.Count; i++)
            {
                sumRate += ViewBag.RvPD[i].Review_Rate;
            }
            double avg = sumRate / ViewBag.RvPD.Count;

            //ViewBag.RateAVG = Math.Round(avg, 1);

            ViewBag.PD = entities.Products.Find(id); // product
            if (ViewBag.RvPD.Count == 0) // neu chua co review gáng gtri mac dinh = Product star
            {
                // ViewBag.RateAVG = ViewBag.PD.Product_Star;
            }
            else
            {
                var oldproduct = entities.Products.Find(id);
                oldproduct.Product_Star = Math.Round(avg, 1);
                //entities.Products.Add(oldproduct);
                entities.SaveChanges();

            }
            //end show review
            ViewBag.PD = entities.Products.Find(id); // product
            ViewBag.PIC = entities.Pictures.Find(id); // picture

            ViewBag.RE = entities.Products.Where(a => a.Product_ID != id).ToList();
            ViewBag.REPIC = entities.Pictures.Where(item => item.Product_ID != id).ToList();

            //get list taste by product id
            ViewBag.Taste = entities.Tastes.Where(item => item.Product_ID == id).ToList();
            //ViewBag.PD = taToOrEntities1.Products.ToList();

            //Show Review
            double user_id = 0;
            user_id = Convert.ToInt32(Session["id"]);// lấy userID từ session và ép kiểu
            var checkCart = entities.Carts.Where(item => item.User_ID == user_id).ToList();
            ViewBag.Review = entities.Reviews.Where(model => model.User_ID == user_id).ToList();
            var listreview = entities.Reviews.Where(model => model.User_ID == user_id).ToList();
            Review a2 = new Review();
            for (int n = 0; n < listreview.Count; n++)
            {
                if (listreview[n].Product_ID == ViewBag.PD.Product_ID)
                {
                    a2 = listreview[n];
                }
            }
            var ListBill = entities.Bills.ToList();// lấy danh sách bill trong database
            ViewBag.checkPro = 0; // nếu check =1 thì hiển form thêm reivew , check =2 thì hiện form sửa review
            var checkReview = entities.Reviews.Where(a => a.User_ID == user_id && a.Product_ID == id).ToList();// kiểm tra review;
            int  checkReviewEditID = 5;
            if (checkReview.Count > 0)
            {
                checkReviewEditID = Convert.ToInt32(checkReview.FirstOrDefault().ReviewEdit_ID);
            }
            // check xem Ngườ dùng đó có bình luận trên product đó chưa
            foreach (var item in checkCart) // chạy dòng for để kiểm tra Cart
            {
                foreach (var bi in ListBill) // dòng for để kiểm tra bill
                {
                    if (item.Cart_ID == bi.Cart_ID) // check xem cartID nào có trong bill 
                    {

                        foreach (var cade in item.CartDetails) // dòng for để kiểm tra CartDetails
                        {
                            if (id == cade.Product_ID && bi.BillStatus_ID == 2 && checkReview.Count() == 0)// check xem sản phẩm nào có trong cartDetail
                            {
                                ViewBag.checkPro = 1; // nếu có thì biến sẽ bằng 1

                                break;

                            }
                            else if (id == cade.Product_ID && bi.BillStatus_ID == 2 && checkReview.Count() > 0 && checkReviewEditID == 1)// check xem sản phẩm nào có trong cartDetail
                            {
                                ViewBag.checkPro = 2; // nếu có thì biến sẽ bằng 1

                                break;

                            }
                        }
                    }
                }
            }
            foreach (var a in checkReview)// dòng for kiểm tra danh sách review
            {
                if (a.ReviewEdit_ID == 1) // được phép sửa review
                {
                    // nếu được thì biến = 2
                    ViewBag.checkPro = 2;
                    break;
                }
            }
            //Chuyển model review sang reviewfile để hiển thị lên trang web
            ReviewFile a3 = new ReviewFile();
            a3.Review_ID = a2.Review_ID;
            a3.User_ID = a2.User_ID;
            a3.Product_ID = a2.Product_ID;
            a3.Review_Rate = a2.Review_Rate;
            a3.Review_Comment = a2.Review_Comment;
            a3.Review_Date = a2.Review_Date;
            a3.ReviewShow_ID = a2.ReviewShow_ID;
            a3.ReviewEdit_ID = a2.ReviewEdit_ID;
            return View(a3);
        }
        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Products/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Delete/5
        public ActionResult Delete(double id)
        {
            //Hàm xoá sản phẩm trong giỏ hàng tại trang show products
            double cartid = Convert.ToDouble(Session["cartid"]);
            var cartdetails = entities.CartDetails.ToList();
            for (int i = 0; i < cartdetails.Count; i++)
            {
                if (cartdetails[i].Cart_ID == cartid && cartdetails[i].Product_ID == id) // điều kiện nếu thoả cartid và products id trong cartdetails thì xoá
                {
                    entities.CartDetails.Remove(cartdetails[i]); // xoá sản phẩm khỏi database
                    entities.SaveChanges(); // lưu lại
                }
            }
            return RedirectToAction("ShowProducts");
        }

        // POST: Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
