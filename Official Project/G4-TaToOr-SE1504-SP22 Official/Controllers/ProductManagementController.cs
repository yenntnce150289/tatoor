﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G4_TaToOr_SE1504_SP22_Official.Models;

namespace G4_TaToOr_SE1504_SP22_Official.Controllers
{
    public class ProductManagementController : Controller
    {
        TaToOrEntities tatoor = new TaToOrEntities();

        // GET: ProductManagement
        public ActionResult ShowAllProduct()
        {
            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                var listProduct = new TaToOrEntities().Products.ToList();
                ViewBag.All = tatoor.Products.Count();
                double id = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = id;
                ViewBag.SoldOutProduct = listProduct.Where(model => model.ProductStatus_ID == 0).ToList();
                ViewBag.AvailableProduct = listProduct.Where(model => model.ProductStatus_ID == 1).ToList();
                ViewBag.DeletedProduct = listProduct.Where(model => model.ProductStatus_ID == 2).ToList();
                return View(listProduct);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }      
        }
        // GET: ProductManagement/Details/5
        public ActionResult Details(int id)
        {
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }

           
        }

        public ActionResult CreateNewProduct()
        {
            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }

           
        }

        // POST: ProductManagement/Create
        [HttpPost]
        public ActionResult CreateNewProduct(Product model1)
        {
            try
            {
                // TODO: Add insert logic here
                var context = new TaToOrEntities();
                context.Products.Add(model1);
                float idcount = context.Products.Count();
                model1.Product_ID = idcount + 1;
                model1.Product_Star = 0;
                model1.ProductStatus_ID = 1;
                context.SaveChanges();
                return RedirectToAction("ShowAllProduct");
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductManagement/Edit/5
        public ActionResult EditProduct(int id)
        {
            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                var context = new TaToOrEntities();
                var editing = context.Products.Find(id);
                editing.Product_Name = editing.Product_Name.Trim();
                editing.Product_Description = editing.Product_Description.Trim();

                return View(editing);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }


        }

        // POST: ProductManagement/Edit/5
        [HttpPost]
        public ActionResult EditProduct(Product model)
        {
            try
            {
                // TODO: Add update logic here
                double type = Convert.ToInt32(Session["TypeOfUser"]);
                ViewBag.Id = type;
                var context = new TaToOrEntities();
                var editing = context.Products.Find(model.Product_ID);
                editing.Product_Name = model.Product_Name;
                editing.Product_Price = model.Product_Price;
                editing.Product_Description = model.Product_Description;
                editing.Product_Star = model.Product_Star;
                editing.ProductStatus_ID = model.ProductStatus_ID;
                editing.Origin_ID = model.Origin_ID;
                editing.TypeOfFood_ID = model.TypeOfFood_ID;
                editing.FormOfFood_ID = model.FormOfFood_ID;
                context.SaveChanges();
                return RedirectToAction("ShowAllProduct");
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductManagement/Delete/5
        // GET: ProductManagement/Delete/5
        public ActionResult DeleteProduct(int id)
        {
            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                var context = new TaToOrEntities();
                var deleteing = context.Products.Find(id);
                return View(deleteing);
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }


        }

        // POST: ProductManagement/Delete/5
        [HttpPost]
        public ActionResult DeleteProduct(Product model)
        {
            try
            {
                // TODO: Add delete logic here
                var context = new TaToOrEntities();
                var olditem = context.Products.Find(model.Product_ID);
                olditem.ProductStatus_ID = 2;
                context.SaveChanges();
                return RedirectToAction("ShowAllProduct");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Upload(int id)
        {
            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                //var context = new TaToOrEntities();
                //var editing = context.Products.Find(id);

                ViewBag.Pic = tatoor.Pictures.Where(i => i.Product_ID == id).ToList();
                ViewBag.idPro = id;
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }

        }


        [HttpGet]
        public ActionResult UploadImage(int id)
        {
            // khong cho user vao trang quan ly
            double typeofuser = 4;
            typeofuser = Convert.ToDouble(Session["TypeOfUser"]);
            double userid = 0;
            userid = Convert.ToDouble(Session["id"]);
            if ((typeofuser == 0 || typeofuser == 1) && userid != 0)
            {

                Session["ID_Product"] = id;
                return View();
            }
            else
            {
                return RedirectToAction("SOS", "Home");
            }

        }


        [HttpPost]
        public ActionResult UploadImage(Picture img)
        {
            var context = new TaToOrEntities();
            //var editing = context.Products.Find(Convert.ToInt32(ID));

            string filename = Path.GetFileNameWithoutExtension(img.ImageFile.FileName);
            string extension = Path.GetExtension(img.ImageFile.FileName);
            filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
            img.Picture_Link = "../../img/" + filename;
            filename = Path.Combine(Server.MapPath("../../img/"), filename);
            img.ImageFile.SaveAs(filename);
            img.Product_ID = Convert.ToInt32(Session["ID_Product"]);
            float idcount = context.Pictures.Count();

            img.Picture_ID = idcount + 1;
            context.Pictures.Add(img);
            context.SaveChanges();



            ModelState.Clear();
            return RedirectToAction("Upload/" + Session["ID_Product"]);
        }
    }
}
